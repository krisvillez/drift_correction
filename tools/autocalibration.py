import numpy as np
import gurobipy as gp
from gurobipy import GRB


def tolerance():
    tol = 1e-3
    return tol


def setupMIQP(Y, theta, lambdas):
    Ym = Y.T - theta[None, :]

    [I, J] = np.shape(Ym)
    L = 100

    m = gp.Model("srs")
    m.setParam(GRB.Param.OutputFlag, False)
    # m.setParam(GRB.Param.OptimalityTol, tolerance())

    gamma = 1e-3
    D = m.addVars(range(J), lb=-np.inf, ub=+np.inf, vtype=GRB.CONTINUOUS, name="d")
    X = m.addVars(range(I), lb=-np.inf, ub=+np.inf, vtype=GRB.CONTINUOUS, name="x")
    Y = m.addVars(range(I), range(J), lb=-np.inf, ub=+np.inf, vtype=GRB.CONTINUOUS, name="y")
    Z = m.addVars(range(I), range(J), lb=0, ub=1, vtype=GRB.INTEGER, name="z")

    # ================================================
    # OBJECTIVE FUNCTION
    obj = gp.QuadExpr()
    for j in range(J):
        obj += gamma * (D[j] ** 2) * I / J
        for i in range(I):
            obj += (Ym[i, j] - D[j] - Y[i, j]) ** 2
            pass
        pass

    m.setObjective(obj)

    for i in range(I):
        for j in range(J):
            # Y_ij = (1-Z_ij) * lambda_i + Z_ij * X_i 
            #  Y_ij >= lambda_j 
            #  Y_ij <= lambda_j + Z_ij*L
            #  Y_ij >= X_i
            #  Y_ij <= X_i + (1-Z_ij)*L

            #  Y_ij >= lambda_j 
            #  Y_ij - Z_ij*L <= lambda_j
            #  Y_ij -X_i >=  0
            #  Y_ij -X_i <= (1-Z_ij)*L

            expr = gp.LinExpr()
            expr += Y[i, j]
            m.addConstr(expr, GRB.GREATER_EQUAL, lambdas[j], name='relax1_' + str(i) + ',' + str(j))

            expr = gp.LinExpr()
            expr += Y[i, j] - Z[i, j] * L
            m.addConstr(expr, GRB.LESS_EQUAL, lambdas[j], name='relax2_' + str(i) + ',' + str(j))

            expr = gp.LinExpr()
            expr += Y[i, j] - X[i]
            m.addConstr(expr, GRB.GREATER_EQUAL, 0, name='relax3_' + str(i) + ',' + str(j))

            expr = gp.LinExpr()
            expr += Y[i, j] - X[i] + Z[i, j] * L
            m.addConstr(expr, GRB.LESS_EQUAL, L, name='relax4_' + str(i) + ',' + str(j))

            pass
        pass

    '''
    for j in range(J): 
        expr = gp.LinExpr()
        expr += D[j]
        
        m.addConstr(expr, GRB.LESS_EQUAL, gamma,
                        name='driftmax_'+str(i))
        
        m.addConstr(expr, GRB.GREATER_EQUAL, -gamma,
                        name='driftmin_'+str(i))
        pass
        '''

    for i in range(I):
        # expr = gp.LinExpr()
        # expr += Z[i,0]
        # m.addConstr(expr, GRB.EQUAL, 1,
        #                name='active0_'+str(i))

        # Z_i,j+1 <= Z_i,j
        #  Z_i,j-Z_i,j+1 >= 0

        expr = gp.LinExpr()
        expr += Z[i, 0] - Z[i, 1]
        m.addConstr(expr, GRB.GREATER_EQUAL, 0,
                    name='active_' + str(i))

        pass

    m.reset()

    return m


class autocal:

    def __init__(self, lambdas, theta_init, window):

        self.lambdas = lambdas
        self.theta = theta_init
        self.theta_diff = theta_init * 0
        self.window = window
        self.data = np.zeros(len(lambdas))[:, None]
        return

    def add_data(self, y):

        data = np.hstack([self.data, y.flatten()[:, None]])
        data = data[:, -self.window:]
        self.data = data

        self.theta_diff = self.theta_diff * 0

        return

    def calibrate(self):

        Y = self.data
        [J, I] = np.shape(Y)
        self.I = I
        self.J = J

        if self.I >= self.window:
            self.Y = Y[:, -self.window:]

            m = setupMIQP(self.Y, self.theta, self.lambdas)
            m.reset()
            m.optimize()

            Dhat = np.zeros(J)
            for j in range(J):
                variable_name = 'd[' + str(j) + ']'
                var = m.getVarByName(variable_name)
                Dhat[j] = var.x
                pass

            self.theta_diff = Dhat
            self.theta = self.theta + Dhat
            self.m = m
            pass
        else:
            pass

        return self.theta

    def get_optimization_results(self):

        I = self.I
        J = self.J
        # PRE-ALLOCATE VALUES FOR ALL OPTIMIZATION VARIABLES
        Yhat = np.zeros([I, J])
        Zhat = np.zeros([I, J])
        Dhat = np.zeros(J)

        for k in range(I):
            for j in range(J):
                variable_name = 'y[' + str(k) + ',' + str(j) + ']'
                var = self.m.getVarByName(variable_name)
                Yhat[k, j] = var.x
                pass
            pass
        Yhat = Yhat.T
        for k in range(I):
            for j in range(J):
                variable_name = 'z[' + str(k) + ',' + str(j) + ']'
                var = self.m.getVarByName(variable_name)
                Zhat[k, j] = var.x
                pass
            pass
        Zhat = Zhat.T

        for j in range(J):
            variable_name = 'd[' + str(j) + ']'
            var = self.m.getVarByName(variable_name)
            Dhat[j] = var.x
            pass

        return Dhat, Zhat, Yhat
