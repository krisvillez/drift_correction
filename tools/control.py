#!/usr/bin/env python3.8 

import numpy as np


class PIcontroller:
    def __init__(self, *, K=1, I=0.1, u_limit=[0, 1], u=0, y_setpoint=0):
        self.K = K
        self.I = I
        self.u_limit = u_limit
        self.u = u
        self.y_setpoint = y_setpoint

        self.e = 0
        return

    def update_setpoint(self, y_setpoint):
        y_setpoint_previous = self.y_setpoint
        self.y_setpoint = y_setpoint

        # bumpless transfer:
        self.e = self.e - (y_setpoint - y_setpoint_previous)
        return

    def apply_input_correction(self, dy_signal):
        e = self.e
        e = e - dy_signal
        self.e = e
        return

    def apply_output_correction(self, du_signal):
        u = self.u
        u_limit = self.u_limit

        u = u - du_signal
        u = np.minimum(u_limit[1], np.maximum(u_limit[0], u))
        self.u = u
        return u

    def compute_control_action(self, y_tilde):
        e_previous = self.e
        u_previous = self.u
        u_limit = self.u_limit

        K = self.K
        I = self.I

        e = y_tilde - self.y_setpoint

        diff_e = e - e_previous

        u = u_previous + K * diff_e + I * e  # unconstrained control actions
        u = np.minimum(u_limit[1], np.maximum(u_limit[0], u))

        self.e = e
        self.u = u
        return u

    pass
