#!/usr/bin/env python3.8
import numpy as np


class pwl:

    def __init__(self, lambdas, Sigma):
        lambdas = np.sort(lambdas)

        num_segment = len(lambdas) + 1

        intervals = np.hstack([-np.inf, lambdas, +np.inf])
        # The sensor responses (y) are given as a piece-wise linear function of the measured variable (x)
        # the vector lambdas marks the values for x that separate the linear segments. 
        # The sensor response for jth segment is described as:
        #  y = Gamma_j . x + Delta_j 
        # The value for j is given as
        #   j = s if L_s <= x < U_s
        #  where L_s , U_s are the bounds for x for which segment s is valid.
        Gamma = np.zeros(len(lambdas))[:, None]
        GAMMA = [Gamma]  # sensor gains
        DELTA = [lambdas[:, None]]  # sensor offset
        L = [intervals[0]]
        U = [intervals[1]]
        for sensor_index, lambdavalue in enumerate(lambdas):
            bool_sensitive = (lambdas <= lambdavalue)
            Gamma = (bool_sensitive * 1.0)[:, None]
            GAMMA.append(Gamma)
            DELTA.append(((1 - bool_sensitive) * lambdas)[:, None])
            L.append(intervals[sensor_index + 1])
            U.append(intervals[sensor_index + 2])
            pass

        # Compute pseudo-inverse of Gamma to speed up computation later (projection)
        P = []
        R = []
        for segment in range(num_segment):
            p = np.linalg.pinv(GAMMA[segment])
            P.append(p)
            p2 = np.vstack([np.linalg.pinv(Gamma), (np.eye(2) - Gamma @ np.linalg.pinv(Gamma))])
            R.append(p2 @ Sigma @ p2.T)

            pass

        self.lambdas = lambdas
        self.L = L
        self.U = U
        self.DELTA = DELTA
        self.GAMMA = GAMMA
        self.P = P
        self.R = R
        self.num_segment = num_segment
        self.Sigma = Sigma

        pass

    def project_and_select(self, y):

        # To determine which segment is most likely, project data (y) onto each segment 
        # to estimate (x), conditional to the segment (s), then select the segment that 
        # delivers the smallest distance from the data point (y) to its projection onto the segment (yhat)

        # Projection step
        Q = []
        RES = []
        XHAT = []
        YHAT = []
        for segment in range(self.num_segment):
            P = self.P[segment]
            L = self.L[segment]
            U = self.U[segment]
            delta = self.DELTA[segment]
            gamma = self.GAMMA[segment]

            xhat = P @ (y[:, None] - delta)  # Naive estimate of measured variable
            # xhat = np.minimum(np.maximum(xhat,L),U) # Constrain the estimate to be within bounds for the considered
            # segment
            # print(xhat.shape,delta.shape,P.shape)
            yhat = gamma @ xhat + delta

            residual = y[:, None] - yhat
            q = np.sum(residual ** 2)

            Q.append(q)
            RES.append(residual)
            XHAT.append(xhat)
            YHAT.append(yhat)

            pass

        index_best_segment = np.argmin(Q)

        xhat_best = XHAT[index_best_segment]
        yhat_best = YHAT[index_best_segment]
        res_best = RES[index_best_segment]
        R = self.R[index_best_segment]

        return xhat_best, yhat_best, res_best, index_best_segment, R, Q

    pass
