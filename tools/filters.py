#!/usr/bin/env python3.8
import numpy as np
from scipy.linalg import block_diag


class HighPassFilter:

    def __init__(self, alpha, y):
        self.alpha = alpha
        self.y_hp = y * 0
        self.y = y
        pass

    def apply_input_correction(self, dy_signal):
        y = self.y
        y = y - dy_signal
        self.y = y

        return

    def update(self, y):
        alpha = self.alpha
        y_hp = self.y_hp
        y_prev = self.y

        y_hp = alpha * y_hp + alpha * (y - y_prev)

        self.y = y
        self.y_hp = y_hp

        return y_hp


class KalmanFilter:
    def __init__(self, *, A, Q, C, R, B=[], D=[]):

        [nY, nX] = C.shape
        if B == []:
            B = np.zeros([nX, 0])
            D = np.zeros([nY, 0])
            pass
        nU = B.shape[1]

        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.Q = Q
        self.R = R

        self.x = np.zeros(nX)
        self.P = np.eye(nX) * 0

        self.num_states = nX
        self.num_inputs = nU
        self.num_outputs = nY
        pass

    def reset(self, *, x0, P0):

        self.x = x0
        self.P = P0
        pass

    def update(self, *, u=[], y):

        xf = self.x
        Pf = self.P
        A = self.A
        B = self.B
        C = self.C
        D = self.D
        Q = self.Q
        R = self.R

        if u == []:
            u = np.zeros(self.num_inputs)
            pass
        # Time update - predict state
        xp = A @ xf + B @ u
        Pp = A @ Pf @ A.T + Q

        # Time update - predict measurement
        yp = C @ xp + D @ u
        Py = C @ Pp @ C.T + R

        # Measurement update - correct state estimate
        prediction_error = yp - y
        K = Pp @ C.T @ np.linalg.inv(Py)
        xf = xp - K @ prediction_error
        Pf = (np.eye(self.num_states) - K @ C) @ Pp

        self.x = xf
        self.P = Pf
        self.yp = yp
        self.Py = Py

        return xf, Pf

    pass


class ukf:

    def __init__(self, *, ffun, hfun, Lx=1, Ly=1, Lv=1, Lw=1, alpha=1, beta=0, kappa=[]):
        L = Lx + Lv + Lw
        print(L)
        nSigmaPoint = 2 * L + 1
        if kappa == []:
            kappa = 3 - L
            pass
        lambda_ = alpha ** 2 * (L + kappa) - L
        zeta = np.sqrt((L + lambda_))

        Wm = 1 / (2 * (L + lambda_)) * np.ones([nSigmaPoint, 1])
        Wm[0] = lambda_ / (L + lambda_)
        Wc = 1 / (2 * (L + lambda_)) * np.ones([nSigmaPoint])
        Wc[0] = lambda_ / (L + lambda_) + (1 - alpha ** 2 + beta)

        self.alpha = alpha
        self.beta = beta
        self.kappa = kappa
        self.lambda_ = lambda_
        self.zeta = zeta

        self.L = L
        self.Lx = Lx
        self.Ly = Ly
        self.Lv = Lv
        self.Lw = Lw
        self.nSigmaPoint = nSigmaPoint

        self.Wm = Wm
        self.Wc = Wc

        self.ffun = ffun
        self.hfun = hfun

        return

    def reset(self, *, xbar=np.array([0]), Pbar=np.array([0])):
        self.xbar = xbar
        self.Pbar = Pbar
        return

    def update(self, *, u=[], ytil=[], theta=[]):

        # print('update')
        L = self.L
        Lx = self.Lx
        Ly = self.Ly
        Lv = self.Lv
        Lw = self.Lw
        Wm = self.Wm
        Wc = self.Wc
        xbar = self.xbar
        Pbar = self.Pbar
        zeta = self.zeta

        ffun = self.ffun
        hfun = self.hfun

        # print('setup')
        nSigmaPoint = self.nSigmaPoint
        xbar2a = np.vstack([xbar, np.zeros([Lv + Lw, 1])])
        # print(xbar2a)
        [V, S, Vt] = np.linalg.svd(Pbar)
        # S1 = V*np.diag(np.sqrt(S)**(1/2)) ;
        S1 = np.linalg.cholesky(Pbar)
        S = block_diag(S1, np.eye(Lv), np.eye(Lw))
        U = np.hstack([np.zeros(L)[:, None], +S, -S])
        Chibar = xbar2a + zeta * U
        Chihat = Chibar

        ## Time update

        # print('time update')
        Ypsilonhat = np.ones([Ly, nSigmaPoint]) * np.nan
        for i in np.arange(nSigmaPoint):
            Chihat[:Lx, i] = ffun(Chibar[:, i], u, theta)
            Ypsilonhat[:, i] = hfun(Chihat[:, i], u, theta)
            pass
        xhat = np.dot(Chihat, Wm)
        # yhat_ref = hfun(xhat[:,0], u, theta) ;
        Dx = Chihat[:Lx, :] - xhat[:Lx, :]
        Phat = np.dot(np.dot(Dx, np.diag(Wc)), Dx.T)

        # print('measurement update')
        if ytil.size > 0:
            yhat = np.dot(Ypsilonhat, Wm)
            Dy = Ypsilonhat - yhat  # [:,None] ;
            Pyhat = np.dot(np.dot(Dy, np.diag(Wc)), Dy.T)

            Pxyhat = np.dot(np.dot(Dx[:Lx, :], np.diag(Wc)), Dy.T)
            K = np.dot(Pxyhat, np.linalg.inv(Pyhat))
            adjustment = np.dot(K, (ytil - yhat))
            # print(adjustment)
            xbar = xhat[:Lx] + adjustment
            Pbar = Phat - np.dot(np.dot(K, Pyhat), K.T)
            pass
        else:
            xbar = xhat[:Lx]
            Pbar = Phat
            pass

        self.xbar = xbar
        self.Pbar = Pbar
        self.yhat = yhat
        # self.yhat_ref = yhat_ref
        return

    pass
