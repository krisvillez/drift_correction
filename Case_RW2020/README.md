This folder contains code to produce results from the following publication:

Chowdhury, D, Melin, A, Villez, K (2021). Automatic Drift Correction through
Nonlinear Sensing. Resilience Week 2021, Washington, DC, USA, September 26–29,
2022. Available at: https://doi.org/10.1109/RWS52686.2021.9611798

To reproduce results, execute the following notebooks (in this order):
- Step1_Simulate_flow_and_liquid_level.ipynb
- Step2_SimulateMeasurementError.ipynb
- Step3c_UKF_ScenarioA.ipynb
- Step3c_UKF_ScenarioB.ipynb
- Step3c_UKF_ScenarioC.ipynb
