#!/usr/bin/env python3.8
import numpy as np
from scipy.integrate import odeint

def PWLin(t,knot,Ak):
    index = np.where(knot<=t)[0][-1]
    return Ak[index]

class modelinput():

    def __init__(self,A0,A,phi,tau,knot,Bk):
        
        num_freq = len(A);
        assert (len(phi) == num_freq),"Length of phi does not match length of A"
        assert (len(tau) == num_freq),"Length of tau does not match length of A"
        assert (len(knot) == len(Bk)),"Length of knot does not match length of B"
        
        self.A0 = A0
        self.A =  A 
        self.phi = phi
        self.tau = tau
        self.num_freq = num_freq
        self.knot = knot
        self.Bk = Bk
        
        return
    
    def simulate(self,t):
        
        A0 = self.A0  
        A = self.A  
        phi = self.phi  
        tau = self.tau  
        
        Bk =  self.Bk
        knot =  self.knot
        B = PWLin(t,knot,Bk)
        
        num_freq = self.num_freq
        Q = A0
        for w in range(num_freq):
            theta = (t-phi[w])/tau[w]*2*np.pi 
            q = A[w]*np.sin(theta)
            if w==0:
                q = B*q
                pass
            Q = Q+q
            pass
        
        return Q
    
    pass
    
    
class tanksystem():
    
    def __init__(self,kappa,tau,vfun,x0):
        
        self.tau = tau
        self.kappa = kappa
        self.vfun = vfun
        self.t = 0
        self.x = x0
        
        return
    
    def eval_ode(self,x, t, u):
        
        kappa = self.kappa
        tau = self.tau
        v = self.vfun(t)
        u = np.minimum(np.maximum(0,u),1)
        dxdt = 1/tau*(v-kappa*u*np.sqrt(x)) 
        
        return dxdt
    
    def simulate(self,timedelta,u):
        
        t0 =self.t
        t1 = t0+timedelta
        tsim = np.linspace(t0,t1,2)
        
        x =self.x
        ffun = lambda x, t,u : self.eval_ode(x, t,u) 
        sol = odeint(ffun, self.x, tsim, args=(u,)) 
        x = sol[:, 0]
        x1 = x[-1] 
        t1 = tsim[-1] 
        self.t = t1
        self.x = x1
        
        return x1
    
    pass
    
    
class sensors():
    
    def __init__(self,delta):
        
        self.delta = delta
        
        return
    
    def simulate(self,x):
        
        y = np.maximum(self.delta,x)
        
        return y
    
    pass
    
    
        
        
        
    