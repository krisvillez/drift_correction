This folder contains code to produce results from the following publication:

Chowdhury, D, Melin, A, Villez, K (accepted). A method for automatic correction of offset driftin online sensors. Water Science and Technology.

To reproduce results, execute the following notebooks (in this order):
- Step1_SimulateError.ipynb
- Step2_Simulation.ipynb
- Step3_Figures.ipynb 